<?php

define("SR_USERNAME", 'paar');
define("SR_APIKEY", '46bc1b5206a24830e7cea481f58cb390'); // meg cserelni kell
define("SR_APIURL", 'http://tesztwebshopsync.api.shoprenter.hu');
define("BH_URL", 'http://tesztwebshopsync.shoprenter.hu');

define("BG_USERNAME", 'attila');
define("BG_APIKEY", '!93O^HgwvqoD#T'); // meg cserelni kell
define("BG_APIURL", 'https://v1.api.brandsgateway.com');
define("BG_URL", 'https://brandsgateway.com');

define('PRODUCT_IMAGE_PATH', 'kepek');
define("SR_DEFAULT_CUSTOMER_GROUP", 'Y3VzdG9tZXJHcm91cC1jdXN0b21lcl9ncm91cF9pZD04'); // Alap user csoport
$custgroup["General"] = "Y3VzdG9tZXJHcm91cC1jdXN0b21lcl9ncm91cF9pZD0xMQ==";
$custgroup["General(30-60)"] = "Y3VzdG9tZXJHcm91cC1jdXN0b21lcl9ncm91cF9pZD0xMg==";
$custgroup["General(60-90)"] = "Y3VzdG9tZXJHcm91cC1jdXN0b21lcl9ncm91cF9pZD0xMw==";
$custgroup["General(90+)"] = "Y3VzdG9tZXJHcm91cC1jdXN0b21lcl9ncm91cF9pZD0xNA==";
$custgroup["Viszonteladó"] = "Y3VzdG9tZXJHcm91cC1jdXN0b21lcl9ncm91cF9pZD0xNQ==";
$custgroup["Viszonteladó(10+)"] = "Y3VzdG9tZXJHcm91cC1jdXN0b21lcl9ncm91cF9pZD0xNg==";
$custgroup["Viszonteladó(1-5)"] = "Y3VzdG9tZXJHcm91cC1jdXN0b21lcl9ncm91cF9pZD0xNw==";
$custgroup["Viszonteladó(5-10)"] = "Y3VzdG9tZXJHcm91cC1jdXN0b21lcl9ncm91cF9pZD0xOA==";
// define ("SR_DEFAULT_CUSTOMER_GROUP_WHOLESALE", 'Y3VzdG9tZXJHcm91cC1jdXN0b21lcl9ncm91cF9pZD05'); // Alap visszonteladoi csoport
define("SR_DEFAULT_PRODUCT_CATEGORY", 'Y2F0ZWdvcnktY2F0ZWdvcnlfaWQ9Njc5');   // Alap kategória

define('PRODUCT_CLASS_ID', 'cHJvZHVjdENsYXNzLXByb2R1Y3RfY2xhc3NfaWQ9OA==');

/*
  General
  General(30-60)
  Viszonteladó(1-5)
  Viszonteladó(10+)
  Viszonteladó
 */

define("LANG_HU", "bGFuZ3VhZ2UtbGFuZ3VhZ2VfaWQ9MQ=="); //global shoprenter value, DO NOT MODIFY
define("TAXCLASS", "dGF4Q2xhc3MtdGF4X2NsYXNzX2lkPTEw"); //global shoprenter value, DO NOT MODIFY

define('API_ENDPOINT_ADDRESS', '/addresses');
define('API_ENDPOINT_BATCH', '/batch');
define('API_ENDPOINT_CUSTOMER', '/customers');
define('API_ENDPOINT_CUSTOMER_GROUP', '/customerGroups');
define('API_ENDPOINT_CUSTOMER_GROUP_PRICE', '/customerGroupProductPrices');
define('API_ENDPOINT_PRODUCT', '/products');
define('API_ENDPOINT_PRODUCT_SPECIAL', '/productSpecials');
define('API_ENDPOINT_PRODUCT_OPTION', '/productOptions');
define('API_ENDPOINT_PRODUCT_OPTION_DESC', '/productOptionDescriptions');
define('API_ENDPOINT_PRODUCT_OPTION_VALUE', '/productOptionValues');
define('API_ENDPOINT_PRODUCT_OPTION_VALUE_DESC', '/productOptionValueDescriptions');
define('API_ENDPOINT_PRODUCT_CLASS', '/productClasses');
define('API_ENDPOINT_PRODUCT_IMAGE', '/productImages');
define('API_ENDPOINT_PRODUCT_DESC', '/productDescriptions');
define('API_ENDPOINT_PRODUCT_TAG', '/productTags');


define('API_ENDPOINT_CATEGORY', '/categories');
define('API_ENDPOINT_URLALIASES', '/urlAliases');

define('API_ENDPOINT_MANUFACTURER', '/manufacturers');
define('API_ENDPOINT_MANUFACTURER_DESC', '/manufacturerDescriptions');

define('API_ENDPOINT_LIST_ATTRIBUTE', '/listAttributes');

define('API_ENDPOINT_COUNTRIES', '/countries');
// attribute_id = 134
$manufacturer["211"] = "Avent";
$manufacturer["196"] = "Dubastar";
$manufacturer["267"] = "Lady-Pharma";
$manufacturer["247"] = "Laica";
$manufacturer["195"] = "Medcare";
$manufacturer["229"] = "Noene";
$manufacturer["243"] = "Obaby";
$manufacturer["278"] = "PER4M";
$manufacturer["21"] = "PÚR";
$manufacturer["23"] = "REEBOK";
$manufacturer["175"] = "RKC";
$manufacturer["190"] = "Salus";
$manufacturer["191"] = "Salus";
$manufacturer["26"] = "Sempermed";
$manufacturer["27"] = "STIL";
$manufacturer["29"] = "Stroops";
$manufacturer["31"] = "Teveclub";
$manufacturer["221"] = "Trendy";
$manufacturer["182"] = "Triggerpoint";
$manufacturer["177"] = "TRX";
$manufacturer["277"] = "Ultimate Sandbag";
$manufacturer["279"] = "X-static";
$manufacturer["248"] = "Zanni";
$manufacturer["3"] = "Albert Hohlkörper GmbH";
$manufacturer["4"] = "Armacell";
$manufacturer["5"] = "Basica";
$manufacturer["6"] = "Beaco";
$manufacturer["7"] = "Bio Baby";
$manufacturer["8"] = "Chicco";
$manufacturer["9"] = "Dittmann";
$manufacturer["10"] = "Easy-bid";
$manufacturer["11"] = "Feelmax";
$manufacturer["12"] = "Frohn";
$manufacturer["13"] = "Gymnic";
$manufacturer["14"] = "Gymstick";
$manufacturer["15"] = "GYSGY Reha";
$manufacturer["16"] = "Holle";
$manufacturer["17"] = "Jessica";
$manufacturer["18"] = "Monapel";
$manufacturer["19"] = "Nike";
$manufacturer["20"] = "POLAR";
$manufacturer["22"] = "R-med Kft.";
$manufacturer["24"] = "Relax Maternity";
$manufacturer["25"] = "Rolyan";
$manufacturer["176"] = "SKLZ";
$manufacturer["28"] = "STOCI";
$manufacturer["30"] = "TEVA";
$manufacturer["32"] = "TOGU";
$manufacturer["33"] = "Uriel";
$manufacturer["281"] = "Xenios";
