<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace resources;

/**
 * Description of ProductClass
 *
 * @author szekus
 */
class ProductClass extends Resource {

    public function __construct() {
        parent::__construct();
        $this->apiEndpoint = "/productClasses";
    }

//    public function insert() {
//        $array = $this->getAsArray();
//        $result = querySRApi($this->apiEndpoint, $array, "POST", "responseBody", true);
//        return $result;
//    }

    public function getAsArray() {
        return array_merge(parent::getAsArray(), get_object_vars($this));
    }

    private $name;
    private $description;
    private $firstVariantSelectType;
    private $secondVariantSelectType;
    private $firstVariantParameter;
    private $secondVariantParameter;
    private $productClassAttributeRelations;

    function getName() {
        return $this->name;
    }

    function getDescription() {
        return $this->description;
    }

    function getFirstVariantSelectType() {
        return $this->firstVariantSelectType;
    }

    function getSecondVariantSelectType() {
        return $this->secondVariantSelectType;
    }

    function getFirstVariantParameter() {
        return $this->firstVariantParameter;
    }

    function getSecondVariantParameter() {
        return $this->secondVariantParameter;
    }

    function getProductClassAttributeRelations() {
        return $this->productClassAttributeRelations;
    }

    function setName($name) {
        $this->name = $name;
    }

    function setDescription($description) {
        $this->description = $description;
    }

    function setFirstVariantSelectType($firstVariantSelectType) {
        $this->firstVariantSelectType = $firstVariantSelectType;
    }

    function setSecondVariantSelectType($secondVariantSelectType) {
        $this->secondVariantSelectType = $secondVariantSelectType;
    }

    function setFirstVariantParameter($firstVariantParameter) {
        $this->firstVariantParameter = $firstVariantParameter;
    }

    function setSecondVariantParameter($secondVariantParameter) {
        $this->secondVariantParameter = $secondVariantParameter;
    }

    function setProductClassAttributeRelations($productClassAttributeRelations) {
        $this->productClassAttributeRelations = $productClassAttributeRelations;
    }

}
