<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace resources;

/**
 *
 * @author tamas
 */
interface IResource {

    const PRODUCT_ARRAY = array("id", "innerId", "sku", "modelNumber", "orderable", "price", "multiplier",
        "multiplierLock", "loyaltyPoints", "stock1", "stock2", "subtractStock", "mainPicture", "width",
        "height", "length", "weight", "status", "imageAlt", "shipped", "minimalOrderNumber", "maximalOrderNumber",
        "minimalOrderNumberMultiply", "availableDate", "quantity", "sortOrder", "freeShipping", "taxClass",
        "noStockStatus", "inStockStatus", "onlyStock1Status", "onlyStock2Status", "productClass", "parentProduct",
        "volumeUnit", "weightUnit", "manufacturer",
    );
    const ORDER_STATUS_ARRAY = array("id", "orderStatusDescriptions");
    const ORDER_STATUS_DESCRITION_ARRAY = array("id", "name", "color", "orderStatus", "language");
    const ORDER_ARRAY = array("id", "innerId", "invoiceId", "invoicePrefix", "firstname",
        "lastname", "phone", "fax", "email", "shippingFirstname", "shippingLastname", "shippingCompany", "shippingAddress1",
        "shippingAddress2", "shippingCity", "shippingPostcode", "shippingCountryName", "shippingZoneName",
        "shippingAddressFormat", "shippingMethodName", "shippingMethodTaxRate", "shippingMethodTaxName", "shippingMethodExtension",
        "paymentFirstname", "paymentLastname", "paymentCompany",
        "paymentAddress1", "paymentAddress2", "paymentCity", "paymentPostcode", "paymentCountryName", "paymentZoneName",
        "paymentAddressFormat", "paymentMethodName", "paymentMethodCode", "paymentMethodTaxRate", "paymentMethodTaxName",
        "paymentMethodAfter", "taxNumber", "comment", "total", "value", "couponTaxRate", "ip", "pickPackPontShopCode",
        "customer", "customerGroup", "shippingCountry", "shippingZone", "paymentCountry", "paymentZone", "orderStatus",
        "language", "currency", "orderTotals", "orderProducts");

}
