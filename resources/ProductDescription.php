<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace resources;

/**
 * Description of ProductDescription
 *
 * @author tamas
 */
class ProductDescription extends Resource {

    public function __construct() {
        parent::__construct();
        $this->apiEndpoint = "/productDescriptions";
    }

    public function getAsArray() {
        return array_merge(parent::getAsArray(), get_object_vars($this));
    }

    private $name;
    private $metaKeywords;
    private $metaDescription;
    private $shortDescription;
    private $description;
    private $customContentTitle;
    private $customContent;
    private $parameters;
    private $packagingUnit;
    private $measurementUnit;
    private $videoCode;
    private $product;
    private $language;

    function getName() {
        return $this->name;
    }

    function getMetaKeywords() {
        return $this->metaKeywords;
    }

    function getMetaDescription() {
        return $this->metaDescription;
    }

    function getShortDescription() {
        return $this->shortDescription;
    }

    function getDescription() {
        return $this->description;
    }

    function getCustomContentTitle() {
        return $this->customContentTitle;
    }

    function getCustomContent() {
        return $this->customContent;
    }

    function getParameters() {
        return $this->parameters;
    }

    function getPackagingUnit() {
        return $this->packagingUnit;
    }

    function getMeasurementUnit() {
        return $this->measurementUnit;
    }

    function getVideoCode() {
        return $this->videoCode;
    }

    function getProduct() {
        return $this->product;
    }

    function getLanguage() {
        return $this->language;
    }

    function setName($name) {
        $this->name = $name;
    }

    function setMetaKeywords($metaKeywords) {
        $this->metaKeywords = $metaKeywords;
    }

    function setMetaDescription($metaDescription) {
        $this->metaDescription = $metaDescription;
    }

    function setShortDescription($shortDescription) {
        $this->shortDescription = $shortDescription;
    }

    function setDescription($description) {
        $this->description = $description;
    }

    function setCustomContentTitle($customContentTitle) {
        $this->customContentTitle = $customContentTitle;
    }

    function setCustomContent($customContent) {
        $this->customContent = $customContent;
    }

    function setParameters($parameters) {
        $this->parameters = $parameters;
    }

    function setPackagingUnit($packagingUnit) {
        $this->packagingUnit = $packagingUnit;
    }

    function setMeasurementUnit($measurementUnit) {
        $this->measurementUnit = $measurementUnit;
    }

    function setVideoCode($videoCode) {
        $this->videoCode = $videoCode;
    }

    function setProduct($product) {
        $this->product = $product;
    }

    function setLanguage($language) {
        $this->language = $language;
    }

}
