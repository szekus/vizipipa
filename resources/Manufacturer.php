<?php

namespace resources;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Manufacturer
 *
 * @author tamas
 */
class Manufacturer extends Resource {

    private $name;
    private $picture;
    private $sortOrder;
    private $robotsMetaTag;
    private $manufacturerDescriptions;

    public function __construct() {
        parent::__construct();
    }

    public function setName($name) {
        $this->name = $name;
    }

    public function getName() {
        return $this->name;
    }

    function getPicture() {
        return $this->picture;
    }

    function getSortOrder() {
        return $this->sortOrder;
    }

    function getRobotsMetaTag() {
        return $this->robotsMetaTag;
    }

    function getManufacturerDescriptions() {
        return $this->manufacturerDescriptions;
    }

    function setPicture($picture) {
        $this->picture = $picture;
    }

    function setSortOrder($sortOrder) {
        $this->sortOrder = $sortOrder;
    }

    function setRobotsMetaTag($robotsMetaTag) {
        $this->robotsMetaTag = $robotsMetaTag;
    }

    function setManufacturerDescriptions($manufacturerDescriptions) {
        $this->manufacturerDescriptions = $manufacturerDescriptions;
    }

    public function getAsArray() {
        return array_merge(parent::getAsArray(),get_object_vars($this));
    }

}
