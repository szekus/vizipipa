<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace resources;

use Exception;

/**
 * Description of Product
 *
 * @author tamas
 */
class Product extends Resource {

    public function createProductCSV() {
        $result = querySRApi($this->apiEndpoint, [], "GET", "responseBody", false);
        $last = $result["last"]["href"];
        $parts = parse_url($last);
        parse_str($parts['query'], $query);
        $lastPage = $query['page'];

        $csvArray = array();
//        $lastPage = 1;
        for ($i = 0; $i <= $lastPage; $i++) {

            $res = querySRApi($this->apiEndpoint . "?page=" . $i, [], "GET", "responseBody", false);
            foreach ($res["items"] as $item) {
                $csv = array();
                $id = getId($item["href"]);
                $resultProduct = querySRApi($this->apiEndpoint . "/" . $id, [], "GET", "responseBody", false);
//                sout($resultProduct);
                $csv["id"] = $resultProduct["id"];
                $csv["innerId"] = $resultProduct["innerId"];
                $csv["sku"] = $resultProduct["sku"];
                $csv["orderable"] = $resultProduct["orderable"];
                $csv["stock1"] = $resultProduct["stock1"];
                $csv["stock2"] = $resultProduct["stock2"];
                $csv["stock3"] = $resultProduct["stock3"];
                $csv["stock4"] = $resultProduct["stock4"];
                $csv["subtractStock"] = $resultProduct["subtractStock"];
                $csv["status"] = $resultProduct["status"];
                $csv["quantity"] = $resultProduct["quantity"];
                $csvArray[] = $csv;
            }
        }
        $headers = array("id", "innerId", "sku", "orderable", "stock1", "stock2", "stock3", "stock4", "subtractStock", "status", "quantity");
        $fp = fopen('data/srProducts.csv', 'w');
        fputcsv($fp, $headers, ";");
        foreach ($csvArray as $fields) {
            fputcsv($fp, array_values($fields), ";");
        }
        fclose($fp);
    }

    public function getProductIdBySku($sku) {
//        sout("sku: " . $sku);
        $sku = urlencode($sku);
//        sout($this->apiEndpoint . "?sku=" . $sku);
        $result = querySRApi($this->apiEndpoint . "?sku=" . $sku, [], "GET", "responseBody", false);
        if (isset($result["items"])) {
//            sout($sku);
            $url = $result["items"][0]["href"];
            $id = $this->getUrlId($url);
            $res = querySRApi($this->apiEndpoint . "/" . $id, [], "GET", "responseBody", false);
            return $res["id"];
        } else {
            sout("nincs sku: ");
            sout($sku);
            sout($result);
            return "NO_PRODUCT";
        }
    }

    public function getProductSkuBySRId($SRId) {
        $result = querySRApi($this->apiEndpoint . "/" . $SRId, [], "GET", "responseBody", false);
        if (key_exists("id", $result)) {
            return $result["sku"];
        } else {
            sout("nincs termek: ");
            sout($SRId);
            sout($result);
            return "NO_PRODUCT";
        }
    }

    public static function create() {
        $resource = new Product();
        $resource->resourceToDB = \db\ResourceToDB::create($resource);
        return $resource;
    }

    public function __construct() {
        parent::__construct();
        $this->apiEndpoint = "/products";
        $this->dataColumns = IResource::PRODUCT_ARRAY;
    }

//    public function insert() {
//        $array = $this->getAsArray();
//        $resultProduct = querySRApi($this->apiEndpoint, $array, "POST", "responseBody", true);
//        return $resultProduct;
//    }

    public function getAsArray() {
        return array_merge(parent::getAsArray(), get_object_vars($this));
    }

//    function getSku() {
//        return $this->sku;
//    }
//
//    function getModelNumber() {
//        return $this->modelNumber;
//    }
//
//    function getOrderable() {
//        return $this->orderable;
//    }
//
//    function getPrice() {
//        return $this->price;
//    }
//
//    function getMultiplier() {
//        return $this->multiplier;
//    }
//
//    function getMultiplierLock() {
//        return $this->multiplierLock;
//    }
//
//    function getLoyaltyPoints() {
//        return $this->loyaltyPoints;
//    }
//
//    function getStock1() {
//        return $this->stock1;
//    }
//
//    function getStock2() {
//        return $this->stock2;
//    }
//
//    function getSubtractStock() {
//        return $this->subtractStock;
//    }
//
//    function getMainPicture() {
//        return $this->mainPicture;
//    }
//
//    function getWidth() {
//        return $this->width;
//    }
//
//    function getHeight() {
//        return $this->height;
//    }
//
//    function getLength() {
//        return $this->length;
//    }
//
//    function getWeight() {
//        return $this->weight;
//    }
//
//    function getStatus() {
//        return $this->status;
//    }
//
//    function getImageAlt() {
//        return $this->imageAlt;
//    }
//
//    function getShipped() {
//        return $this->shipped;
//    }
//
//    function getMinimalOrderNumber() {
//        return $this->minimalOrderNumber;
//    }
//
//    function getMaximalOrderNumber() {
//        return $this->maximalOrderNumber;
//    }
//
//    function getMinimalOrderNumberMultiply() {
//        return $this->minimalOrderNumberMultiply;
//    }
//
//    function getAvailableDate() {
//        return $this->availableDate;
//    }
//
//    function getQuantity() {
//        return $this->quantity;
//    }
//
//    function getSortOrder() {
//        return $this->sortOrder;
//    }
//
//    function getFreeShipping() {
//        return $this->freeShipping;
//    }
//
//    function getTaxClass() {
//        return $this->taxClass;
//    }
//
//    function getNoStockStatus() {
//        return $this->noStockStatus;
//    }
//
//    function getInStockStatus() {
//        return $this->inStockStatus;
//    }
//
//    function getOnlyStock1Status() {
//        return $this->onlyStock1Status;
//    }
//
//    function getOnlyStock2Status() {
//        return $this->onlyStock2Status;
//    }
//
//    function getProductClass() {
//        return $this->productClass;
//    }
//
//    function getParentProduct() {
//        return $this->parentProduct;
//    }
//
//    function getVolumeUnit() {
//        return $this->volumeUnit;
//    }
//
//    function getWeightUnit() {
//        return $this->weightUnit;
//    }
//
//    function getManufacturer() {
//        return $this->manufacturer;
//    }
//
//    function setSku($sku) {
//        $this->sku = $sku;
//    }
//
//    function setModelNumber($modelNumber) {
//        $this->modelNumber = $modelNumber;
//    }
//
//    function setOrderable($orderable) {
//        $this->orderable = $orderable;
//    }
//
//    function setPrice($price) {
//        $this->price = $price;
//    }
//
//    function setMultiplier($multiplier) {
//        $this->multiplier = $multiplier;
//    }
//
//    function setMultiplierLock($multiplierLock) {
//        $this->multiplierLock = $multiplierLock;
//    }
//
//    function setLoyaltyPoints($loyaltyPoints) {
//        $this->loyaltyPoints = $loyaltyPoints;
//    }
//
//    function setStock1($stock1) {
//        $this->stock1 = $stock1;
//    }
//
//    function setStock2($stock2) {
//        $this->stock2 = $stock2;
//    }
//
//    function setSubtractStock($subtractStock) {
//        $this->subtractStock = $subtractStock;
//    }
//
//    function setMainPicture($mainPicture) {
//        $this->mainPicture = $mainPicture;
//    }
//
//    function setWidth($width) {
//        $this->width = $width;
//    }
//
//    function setHeight($height) {
//        $this->height = $height;
//    }
//
//    function setLength($length) {
//        $this->length = $length;
//    }
//
//    function setWeight($weight) {
//        $this->weight = $weight;
//    }
//
//    function setStatus($status) {
//        $this->status = $status;
//    }
//
//    function setImageAlt($imageAlt) {
//        $this->imageAlt = $imageAlt;
//    }
//
//    function setShipped($shipped) {
//        $this->shipped = $shipped;
//    }
//
//    function setMinimalOrderNumber($minimalOrderNumber) {
//        $this->minimalOrderNumber = $minimalOrderNumber;
//    }
//
//    function setMaximalOrderNumber($maximalOrderNumber) {
//        $this->maximalOrderNumber = $maximalOrderNumber;
//    }
//
//    function setMinimalOrderNumberMultiply($minimalOrderNumberMultiply) {
//        $this->minimalOrderNumberMultiply = $minimalOrderNumberMultiply;
//    }
//
//    function setAvailableDate($availableDate) {
//        $this->availableDate = $availableDate;
//    }
//
//    function setQuantity($quantity) {
//        $this->quantity = $quantity;
//    }
//
//    function setSortOrder($sortOrder) {
//        $this->sortOrder = $sortOrder;
//    }
//
//    function setFreeShipping($freeShipping) {
//        $this->freeShipping = $freeShipping;
//    }
//
//    function setTaxClass($taxClass) {
//        $this->taxClass = $taxClass;
//    }
//
//    function setNoStockStatus($noStockStatus) {
//        $this->noStockStatus = $noStockStatus;
//    }
//
//    function setInStockStatus($inStockStatus) {
//        $this->inStockStatus = $inStockStatus;
//    }
//
//    function setOnlyStock1Status($onlyStock1Status) {
//        $this->onlyStock1Status = $onlyStock1Status;
//    }
//
//    function setOnlyStock2Status($onlyStock2Status) {
//        $this->onlyStock2Status = $onlyStock2Status;
//    }
//
//    function setProductClass($productClass) {
//        $this->productClass = $productClass;
//    }
//
//    function setParentProduct($parentProduct) {
//        $this->parentProduct = $parentProduct;
//    }
//
//    function setVolumeUnit($volumeUnit) {
//        $this->volumeUnit = $volumeUnit;
//    }
//
//    function setWeightUnit($weightUnit) {
//        $this->weightUnit = $weightUnit;
//    }
//
//    function setManufacturer($manufacturer) {
//        $this->manufacturer = $manufacturer;
//    }

    

}
