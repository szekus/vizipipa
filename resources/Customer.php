<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace resources;

/**
 * Description of Costumer
 *
 * @author tamas
 */
class Customer extends Resource {

    private $firstname;
    private $lastname;
    private $email;
    private $telephone;
    private $fax;
    private $password;
    private $newsletter;
    private $status;
    private $approved;
    private $freeShipping;
    private $defaultAdresses;
    private $customerGroup;
    private $loyaltyPoints;
    private $addresses = [];

    public function __construct() {
        parent::__construct();
    }

    function getFirstname() {
        return $this->firstname;
    }

    function getLastname() {
        return $this->lastname;
    }

    function getEmail() {
        return $this->email;
    }

    function getTelephone() {
        return $this->telephone;
    }

    function getFax() {
        return $this->fax;
    }

    function getPassword() {
        return $this->password;
    }

    function getNewsletter() {
        return $this->newsletter;
    }

    function getStatus() {
        return $this->status;
    }

    function getApproved() {
        return $this->approved;
    }

    function getFreeShipping() {
        return $this->freeShipping;
    }

    function getDefaultAdresses() {
        return $this->defaultAdresses;
    }

    function getCustomerGrooup() {
        return $this->customerGrooup;
    }

    function getLoyaltyPoints() {
        return $this->loyaltyPoints;
    }

    function setFirstname($firstname) {
        $this->firstname = $firstname;
    }

    function setLastname($lastname) {
        $this->lastname = $lastname;
    }

    function setEmail($email) {
        $this->email = $email;
    }

    function setTelephone($telephone) {
        $this->telephone = $telephone;
    }

    function setFax($fax) {
        $this->fax = $fax;
    }

    function setPassword($password) {
        $this->password = $password;
    }

    function setNewsletter($newsletter) {
        $this->newsletter = $newsletter;
    }

    function setStatus($status) {
        $this->status = $status;
    }

    function setApproved($approved) {
        $this->approved = $approved;
    }

    function setFreeShipping($freeShipping) {
        $this->freeShipping = $freeShipping;
    }

    function setDefaultAdresses($defaultAdresses) {
        $this->defaultAdresses = $defaultAdresses;
    }

    function setCustomerGroup($customerGroup) {
        $this->customerGroup = $customerGroup;
    }

    function setLoyaltyPoints($loyaltyPoints) {
        $this->loyaltyPoints = $loyaltyPoints;
    }
    
    function getAddresses() {
        return $this->addresses;
    }

    function addAddress($address) {
        $this->addresses[] = $address;
    }

    
    public function getAsArray() {
        return array_merge(parent::getAsArray(), get_object_vars($this));
    }

}
