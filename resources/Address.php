<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace resources;

/**
 * Description of Address
 *
 * @author tamas
 */
class Address extends Resource {

    private $company;
    private $firstname;
    private $lastname;
    private $taxNumber;
    private $address1;
    private $address2;
    private $postcode;
    private $city;
    private $customer;
    private $country;
    private $zone;

    public function __construct() {
        parent::__construct();
    }

    function getCompany() {
        return $this->company;
    }

    function getFirstname() {
        return $this->firstname;
    }

    function getLastname() {
        return $this->lastname;
    }

    function getTaxNumber() {
        return $this->taxNumber;
    }

    function getAddress1() {
        return $this->address1;
    }

    function getAddress2() {
        return $this->address2;
    }

    function getPostcode() {
        return $this->postcode;
    }

    function getCity() {
        return $this->city;
    }

    function getCustomer() {
        return $this->customer;
    }

    function getCountry() {
        return $this->country;
    }

    function getZone() {
        return $this->zone;
    }

    function setCompany($company) {
        $this->company = $company;
    }

    function setFirstname($firstname) {
        $this->firstname = $firstname;
    }

    function setLastname($lastname) {
        $this->lastname = $lastname;
    }

    function setTaxNumber($taxNumber) {
        $this->taxNumber = $taxNumber;
    }

    function setAddress1($address1) {
        $this->address1 = $address1;
    }

    function setAddress2($address2) {
        $this->address2 = $address2;
    }

    function setPostcode($postcode) {
        $this->postcode = $postcode;
    }

    function setCity($city) {
        $this->city = $city;
    }

    function setCustomer($customer) {
        $this->customer = $customer;
    }

    function setCountry($country) {
        $this->country = $country;
    }

    function setZone($zone) {
        $this->zone = $zone;
    }

    public function getAsArray() {
        return array_merge(parent::getAsArray(), get_object_vars($this));
    }

}
