<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace resources;

/**
 * Description of ProductImage
 *
 * @author szekus
 */
class ProductImage extends Resource {

    private $imagePath;
    private $imageAlt;
    private $product;

    public function __construct() {
        parent::__construct();
    }

    public function getAsArray() {
        return array_merge(parent::getAsArray(), get_object_vars($this));
    }
    
    
    function getImagePath() {
        return $this->imagePath;
    }

    function getImageAlt() {
        return $this->imageAlt;
    }

    function getProduct() {
        return $this->product;
    }

    function setImagePath($imagePath) {
        $this->imagePath = $imagePath;
    }

    function setImageAlt($imageAlt) {
        $this->imageAlt = $imageAlt;
    }

    function setProduct($product) {
        $this->product = $product;
    }


}
