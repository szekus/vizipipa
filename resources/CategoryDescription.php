<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace resources;

/**
 * Description of CategoryDescription
 *
 * @author tamas
 */
class CategoryDescription extends Resource {

    private $name;
    private $metaKeywords;
    private $metaDescription;
    private $description;
    private $customTitle;
    private $robotsMetaTag;
    private $footerSeoText;
    private $category;
    private $language;

    public function __construct() {
        parent::__construct();
    }

    function getName() {
        return $this->name;
    }

    function getMetaKeywords() {
        return $this->metaKeywords;
    }

    function getMetaDescription() {
        return $this->metaDescription;
    }

    function getDescription() {
        return $this->description;
    }

    function getCustomTitle() {
        return $this->customTitle;
    }

    function getRobotsMetaTag() {
        return $this->robotsMetaTag;
    }

    function getFooterSeoText() {
        return $this->footerSeoText;
    }

    function getCategory() {
        return $this->category;
    }

    function getLanguage() {
        return $this->language;
    }

    function setName($name) {
        $this->name = $name;
    }

    function setMetaKeywords($metaKeywords) {
        $this->metaKeywords = $metaKeywords;
    }

    function setMetaDescription($metaDescription) {
        $this->metaDescription = $metaDescription;
    }

    function setDescription($description) {
        $this->description = $description;
    }

    function setCustomTitle($customTitle) {
        $this->customTitle = $customTitle;
    }

    function setRobotsMetaTag($robotsMetaTag) {
        $this->robotsMetaTag = $robotsMetaTag;
    }

    function setFooterSeoText($footerSeoText) {
        $this->footerSeoText = $footerSeoText;
    }

    function setCategory($category) {
        $this->category = $category;
    }

    function setLanguage($language) {
        $this->language = $language;
    }

    public function getAsArray() {
        return array_merge(parent::getAsArray(), get_object_vars($this));
    }

}
