<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace resources;

/**
 * Description of ProductSpecial
 *
 * @author tamas
 */
class ProductSpecial extends Resource {

    public function __construct() {
        parent::__construct();
    }

    public function getAsArray() {
        return array_merge(parent::getAsArray(), get_object_vars($this));
    }

    private $priority;
    private $price;
    private $dateFrom;
    private $dateTo;
    private $minQuantity;
    private $maxQuantity;
    private $product;
    private $customerGroup;

    function getPriority() {
        return $this->priority;
    }

    function getPrice() {
        return $this->price;
    }

    function getDateFrom() {
        return $this->dateFrom;
    }

    function getDateTo() {
        return $this->dateTo;
    }

    function getMinQuantity() {
        return $this->minQuantity;
    }

    function getMaxQuantity() {
        return $this->maxQuantity;
    }

    function getProduct() {
        return $this->product;
    }

    function getCustomerGroup() {
        return $this->customerGroup;
    }

    function setPriority($priority) {
        $this->priority = $priority;
    }

    function setPrice($price) {
        $this->price = $price;
    }

    function setDateFrom($dateFrom) {
        $this->dateFrom = $dateFrom;
    }

    function setDateTo($dateTo) {
        $this->dateTo = $dateTo;
    }

    function setMinQuantity($minQuantity) {
        $this->minQuantity = $minQuantity;
    }

    function setMaxQuantity($maxQuantity) {
        $this->maxQuantity = $maxQuantity;
    }

    function setProduct($product) {
        $this->product = $product;
    }

    function setCustomerGroup($customerGroup) {
        $this->customerGroup = $customerGroup;
    }

}
