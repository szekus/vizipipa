<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace helpers;

/**
 * Description of Helper
 *
 * @author szekus
 */
class Helper {

    protected $db;
    protected $limit = 5;
    protected $counter = 0;

    public function __construct() {
        $this->db = \db\Database::instance();
    }

    function getId($url) {
        $arr = explode("/", $url);
        return $arr[count($arr) - 1];
    }

}
