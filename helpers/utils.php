<?php

function pre_print($printable) {
    echo '<pre>';
    print_r($printable);
    echo '</pre>';
}

function slug($string, $space = "-") {
    // $string = utf8_encode($string);

    /*
      if (function_exists('iconv')) {
      $string = iconv('UTF-8', 'ASCII//TRANSLIT', $string);
      } */
    $string = str_replace(['Á', 'É', 'Í', 'Ő', 'Ú', 'Ó', 'Ö', 'Ű', 'Ü', 'í', 'ü', 'ó', 'ő', 'ú', 'á', 'é', 'ű'], ['A', 'E', 'I', 'O', 'U', 'O', 'O', 'U', 'U', 'i', 'u', 'o', 'o', 'u', 'a', 'e', 'u'], $string);
    $string = preg_replace("/[^a-zA-Z0-9 \-]/", "", $string);
    $string = preg_replace('/-/', ' ', $string);
    $string = trim(preg_replace("/\\s+/", " ", $string));
    $string = strtolower($string);
    $string = str_replace(" ", $space, $string);

    return $string;
}

function bolthelyOptionsExplode($optionsRow) {
    $options = [];
    $propparts = explode('*', $optionsRow);
    if (is_array($propparts) && count($propparts) > 0) {
        $propname = array_shift($propparts);
        foreach ($propparts as $prop) {
            $options[$propname][$prop] = $prop; // 1 prop egyszer
        }
    }
    return $options;
}

function randomPassword() {
    $alphabet = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890';
    $pass = array(); //remember to declare $pass as an array
    $alphaLength = strlen($alphabet) - 1; //put the length -1 in cache
    for ($i = 0; $i < 8; $i++) {
        $n = rand(0, $alphaLength);
        $pass[] = $alphabet[$n];
    }
    return implode($pass); //turn the array into a string
}

function getNetPrice($price, $tax) {
    return $price / (1.00 + $tax / 100);
}

function sout($param) {
    echo "\n";
    var_dump($param);
    echo "\n";
}

function getId($url) {
    $arr = explode("/", $url);
    return $arr[count($arr) - 1];
}

function getParamValue($url, $param) {
    $parts = parse_url($url);
    parse_str($parts['query'], $query);
    return $query[$param];
}

function removeAccent($string) {
    $from = array("Á", "É", "Í", "Ó", "Ö", "Ő", "Ú", "Ü", "Ű", "Ó", "O", "U", "Ü", "á", "é", "í", "ó", "ö", "ő", "ú", "ü", "ű", "ó");
    $to = array("A", "E", "I", "O", "O", "O", "U", "U", "U", "O", "O", "U", "U", "a", "e", "i", "o", "o", "o", "u", "u", "u", "o");
    $string = str_replace($from, $to, $string);
    return $string;
}

function createArrayFromCSV($fileUrl) {
    $array = array_map('str_getcsv', file($fileUrl));
    $header = explode(";", $array[0][0]);
//    sout($array);
    $result = array();
    for ($i = 1; $i < count($array); $i++) {
        $data = array();
        $tmp = explode(";", $array[$i][0]);
//        sout($tmp);
//        die();
        for ($j = 0; $j < (count($header)); $j++) {
            $data[$header[$j]] = $tmp[$j];
        }
        $result[] = $data;
    }

    return $result;
}

function getApiEndpointFromHref($href){
    $arr = explode("/", $href);
    return $arr[count($arr)-1];
}
