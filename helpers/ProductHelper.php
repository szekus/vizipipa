<?php

namespace helpers;

define('RAKTARON', 'c3RvY2tTdGF0dXMtc3RvY2tfc3RhdHVzX2lkPTk=');
define('KESZLETHIANY', 'c3RvY2tTdGF0dXMtc3RvY2tfc3RhdHVzX2lkPTEz');
define('SZALLITAS_1_NAP', 'c3RvY2tTdGF0dXMtc3RvY2tfc3RhdHVzX2lkPTE0');
define('SZALLITAS_TIZ_NAP', 'c3RvY2tTdGF0dXMtc3RvY2tfc3RhdHVzX2lkPTE1');
define('SZALLITAS_24_ORA', 'c3RvY2tTdGF0dXMtc3RvY2tfc3RhdHVzX2lkPTE2');

use resources;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of ProductHelper
 *
 * @author szekus
 */
class ProductHelper extends Helper {

    protected $products;
    protected $productClass;
    protected $childProducts;

    public function __construct() {
        parent::__construct();
        $this->limit = -1;
    }

    function getProducts() {
        return $this->products;
    }

    function setProducts($products) {
        $this->products = $products;
    }

    function getChildProducts() {
        return $this->childProducts;
    }

    function getProductClass() {
        return $this->productClass;
    }

    function setProductClass() {
        $array = array_map('str_getcsv', file('data/productClass.csv'));
        $header = explode(";", $array[0][0]);


        for ($i = 1; $i < count($array); $i++) {
            $class = array();
            $tmp = explode(";", $array[$i][0]);
            for ($j = 0; $j < count($header); $j++) {
                $class[$header[$j]] = $tmp[$j];
            }

            $name = $class["name"];
            $arr = explode("-", $name);
            $original_id = $arr[count($arr) - 1];
            $class["original_id"] = $original_id;

            $this->productClass[] = $class;
        }
    }

    function setChildProducts() {
        $array = array_map('str_getcsv', file('data/childProducts.csv'));
        $header = explode(";", $array[0][0]);


        for ($i = 1; $i < count($array); $i++) {
            $child = array();
            $tmp = explode(";", $array[$i][0]);
            for ($j = 0; $j < count($header); $j++) {
                $child[$header[$j]] = $tmp[$j];
            }

            $this->childProducts[] = $child;
        }
    }

    public function createChildProduct() {


        $query = "SELECT id_product, count(*) AS c FROM ps_product_attribute GROUP BY id_product HAVING c >= 1";
        $result = $this->db->query($query);
        $csvArray = array();
        foreach ($result as $value) {
            $productId = $value["id_product"];
            $query = "SELECT * FROM ps_product_attribute WHERE id_product = " . $productId;

            $childProducts = $this->db->query($query);
            $csv = array();
            $csv["parent_id"] = $value["id_product"];

            $i = 0;
            foreach ($childProducts as $child) {
                $i++;
                $productResource = new resources\Product();


                $csv["id_product_attribute"] = $child["id_product_attribute"];
                $csv["increment"] = $i;

                $query = "SELECT * FROM ps_product AS p "
                        . "INNER JOIN ps_product_lang AS pl ON pl.id_product = p.id_product "
                        . "WHERE pl.id_lang = 6 AND p.id_product = " . $productId;

                $productQueryResult = $this->db->query($query);
                $product = null;
                foreach ($productQueryResult as $productQueryResultValue) {
                    $product = $productQueryResultValue;
                }

                $reference = $child["reference"];
                $sku = $reference;
                if ($reference == "") {
                    $sku = $productId . "_" . $i;
                } else if ($reference == $product["reference"]) {
                    $sku = $reference . "_" . $i;
                }
                $csv["sku"] = $sku;
                $productResource->setSku($sku);
                $productResource->setModelNumber($child["supplier_reference"]);

                $productResource->setOrderable(1);

                $price = $child["price"];
                if ($price == 0) {
                    $price = $product["price"];
                }
                $productResource->setPrice($price);

                $query_3 = "SELECT quantity FROM ps_stock_mvt WHERE id_product = " . $productId
                        . " AND id_product_attribute = " . $child["id_product_attribute"];
                $stock = $this->db->findOneByQuery($query_3, "quantity");
                if ($stock == "") {
                    $stock = 100;
                }
                $productResource->setStock1($stock);

                $productResource->setStatus($product["active"]);

                $productResource->setTaxClass(["id" => TAX_27_CLASS]);
                $productResource->setAvailableDate($product["date_add"]);

                if ($product["id_manufacturer"] != 0) {
                    $manufacturerSRId = base64_encode("manufacturer-manufacturer_id=" . $product["id_manufacturer"]);
                    $productResource->setManufacturer(["id" => $manufacturerSRId]);
                }


                $query = "SELECT id_image FROM ps_product_attribute_image WHERE id_product_attribute = " . $child["id_product_attribute"] . " LIMIT 1";
                $imageId = $this->db->findOneByQuery($query, "id_image");
                if ($imageId != "") {
                    $mainPicture = $this->createImageUrlFromImageId($imageId);
                    $productResource->setMainPicture($mainPicture);
                    $query = "SELECT legend FROM ps_image_lang WHERE id_image = " . $imageId . " AND id_lang = 6";
                    $imageAlt = $this->db->findOneByQuery($query, "legend");
                    $productResource->setImageAlt($imageAlt);
                } else {
                    $query = "SELECT id_image FROM ps_image WHERE id_product = " . $productId . " LIMIT 1";
                    $mainImageId = $this->db->findOneByQuery($query, "id_image");
                    if ($mainImageId != "") {
                        $mainPicture = $this->createImageUrlFromImageId($mainImageId);
                        $productResource->setMainPicture($mainPicture);
                        $query = "SELECT legend FROM ps_image_lang WHERE id_image = " . $mainImageId . " AND id_lang = 6";
                        $imageAlt = $this->db->findOneByQuery($query, "legend");
                        $productResource->setImageAlt($imageAlt);
                    }
                }


                $resultProduct = $productResource->insert();
                $csv["child_sr_id"] = $resultProduct["id"];
                $query = "SELECT id_category FROM ps_category_product WHERE id_product = " . $productId;
                $categories = $this->db->query($query);
                if ($categories != FALSE) {
                    foreach ($categories as $cat) {
                        $categoryId = $cat["id_category"];

                        $categorySRId = base64_encode("category-category_id=" . $categoryId);
                        $relation = Array
                            (
                            'product' => Array
                                (
                                'id' => $resultProduct["id"]
                            ),
                            'category' => Array
                                (
                                'id' => $categorySRId
                            )
                        );

                        $response = querySRApi("/productCategoryRelations", $relation, 'POST', "responseBody", false);
                    }
                }

                $productDescritionResource = new resources\ProductDescription();
                $productDescritionResource->setName($product["name"]);
                $productDescritionResource->setMetaKeywords($product["meta_keywords"]);
                $productDescritionResource->setMetaDescription($product["meta_description"]);
                $productDescritionResource->setShortDescription($product["description_short"]);

                $description = $product["description"];

                $query = "SELECT * FROM ps_feature_product WHERE id_product = " . $productId;
                $result = $this->db->query($query);
                if ($result != false) {
                    $additionalInformation = "\n\n";
                    foreach ($result as $value) {
                        $query_2 = "SELECT name FROM ps_feature_lang WHERE id_lang = 6 AND id_feature = " . $value["id_feature"];
                        $name = $this->db->findOneByQuery($query_2, "name");

                        $query_3 = "SELECT value FROM ps_feature_value_lang WHERE id_lang = 6 AND id_feature_value = " . $value["id_feature_value"];
                        $value = $this->db->findOneByQuery($query_3, "value");

                        $additionalInformation .= $name . " " . $value . "\n\n";
                    }
                    $description .= $additionalInformation;
                }

                $productDescritionResource->setDescription($description);

                $productDescritionResource->setProduct(["id" => $resultProduct["id"]]);
                $productDescritionResource->setLanguage(["id" => LANG_HU]);

                $productDescritionResource->insert();
                $csvArray[] = $csv;
            }
        }
        $headers = ['parent_id', 'id_product_attribute', 'increment', 'sku', 'child_sr_id'];
        $fp = fopen('data/childProducts.csv', 'w');
        fputcsv($fp, $headers, ";");
        foreach ($csvArray as $fields) {
            fputcsv($fp, array_values($fields), ";");
        }
        fclose($fp);
    }

    public function insertProducts() {
//insert products
        foreach ($this->products as $product) {
            $this->counter++;
            $productResource = new resources\Product();
            $productId = $product["id_product"];
            $productSRId = base64_encode("product-product_id=" . $productId);
            $productResource->setId($productId);
            $productResource->setInnerId($productId);

            $productResource->setSubtractStock(0);
            $productResource->setStock1(100);


            $reference = $product["reference"];
            $sku = $reference;
            if ($reference == "") {
                $sku = $productId;
            } else {
                $query = "SELECT count(*) AS counter FROM ps_product WHERE reference = '" . $reference . "'";
                $numberOfSku = $this->db->findOneByQuery($query, "counter");
                if ($numberOfSku > 1 && $reference != "") {
                    $sku = $reference . "-" . $productId;
                }
            }

            $productResource->setSku($sku);
            $productResource->setModelNumber($product["supplier_reference"]);

            $productResource->setOrderable(1);
            $productResource->setPrice($product["price"]);
            $productResource->setStatus($product["active"]);

            $productResource->setTaxClass(["id" => TAX_27_CLASS]);
            $productResource->setAvailableDate($product["date_add"]);

            if ($product["id_manufacturer"] != 0) {
                $manufacturerSRId = base64_encode("manufacturer-manufacturer_id=" . $product["id_manufacturer"]);
                $productResource->setManufacturer(["id" => $manufacturerSRId]);
            }



            $query = "SELECT id_image FROM ps_image WHERE id_product = " . $productId . " LIMIT 1";
            $images = $this->db->query($query);
            $imageId = -1;
            if ($images != false) {
                foreach ($images as $img) {
                    $imageId = $img["id_image"];
                    $mainPicture = $this->createImageUrlFromImageId($imageId);
                    $productResource->setMainPicture($mainPicture);
                }
            }


            if ($imageId > 0) {
                $query = "SELECT legend FROM ps_image_lang WHERE id_image = " . $imageId . " AND id_lang = 6";
                $imageAlt = $this->db->findOneByQuery($query, "legend");
                if ($imageAlt != NULL) {
                    $productResource->setImageAlt($imageAlt);
                }
            }



            $array = $productResource->getAsArray();
            $resultProduct = querySRApi(API_ENDPOINT_PRODUCT, $array, "POST", "responseBody", false);


            $query = "SELECT id_category FROM ps_category_product WHERE id_product = " . $productId;
            $categories = $this->db->query($query);
            if ($categories != FALSE) {
                foreach ($categories as $cat) {
                    $categoryId = $cat["id_category"];

                    $categorySRId = base64_encode("category-category_id=" . $categoryId);
                    $relation = Array
                        (
                        'product' => Array
                            (
                            'id' => $productSRId
                        ),
                        'category' => Array
                            (
                            'id' => $categorySRId
                        )
                    );

                    $response = querySRApi("/productCategoryRelations", $relation, 'POST', "responseBody", true);
                }
            }


            if ($this->counter == $this->limit) {
                die();
            }
        }
    }

    public function insertProductDescription() {
        foreach ($this->products as $product) {
            $productId = $product["id_product"];
            $productSRId = base64_encode("product-product_id=" . $productId);
            $productDescritionResource = new resources\ProductDescription();
            $productDescritionResource->setName($product["name"]);
            $productDescritionResource->setMetaKeywords($product["meta_keywords"]);
            $productDescritionResource->setMetaDescription($product["meta_description"]);
            $productDescritionResource->setShortDescription($product["description_short"]);

            $description = $product["description"];

            $query = "SELECT * FROM ps_feature_product WHERE id_product = " . $productId;
            $result = $this->db->query($query);
            if ($result != false) {
                $additionalInformation = "\n\n";
                foreach ($result as $value) {
                    $query_2 = "SELECT name FROM ps_feature_lang WHERE id_lang = 6 AND id_feature = " . $value["id_feature"];
                    $name = $this->db->findOneByQuery($query_2, "name");

                    $query_3 = "SELECT value FROM ps_feature_value_lang WHERE id_lang = 6 AND id_feature_value = " . $value["id_feature_value"];
                    $value = $this->db->findOneByQuery($query_3, "value");

                    $additionalInformation .= $name . " " . $value . "\n\n";
                }
                $description .= $additionalInformation;
            }

            $productDescritionResource->setDescription($description);

            $productDescritionResource->setProduct(["id" => $productSRId]);
            $productDescritionResource->setLanguage(["id" => LANG_HU]);

            $productDescritionResource->insert();

//            $array = ($productDescritionResource->getAsArray());
//            sout($array);
//            $resultProductDescrition = querySRApi(API_ENDPOINT_PRODUCT_DESC, $array, "POST", "responseBody", true);
        }
    }

    public function insertProductsImage() {
        foreach ($this->products as $product) {
            $productId = $product["id_product"];
            $productSRId = base64_encode("product-product_id=" . $productId);

            $query = "SELECT id_image FROM ps_image WHERE id_product = " . $productId;
            $images = $this->db->query($query);
            if ($images != false) {
                foreach ($images as $img) {
                    $productImageResource = new resources\ProductImage();
                    $imageId = $img["id_image"];
                    $imgPath = $this->createImageUrlFromImageId($imageId);
                    $productImageResource->setImagePath($imgPath);

                    $query = "SELECT legend FROM ps_image_lang WHERE id_image = " . $imageId . " AND id_lang = 6";
                    $imageAlt = $this->db->findOneByQuery($query, "legend");
                    $productImageResource->setImageAlt($imageAlt);
                    $productImageResource->setProduct(["id" => $productSRId]);
                    $array = ($productImageResource->getAsArray());
//                    sout($array);
                    $resultProductImage = querySRApi(API_ENDPOINT_PRODUCT_IMAGE, $array, "POST", "responseBody", true);
                }
            }
        }
    }

    public function inserProductSpecial() {
        foreach ($this->products as $product) {
            $productId = $product["id_product"];
            $productSRId = base64_encode("product-product_id=" . $productId);
            $productSpecialResource = new resources\ProductSpecial();



            $query = "SELECT * FROM ps_specific_price AS sp WHERE id_product = " . $productId . " ORDER BY sp.from desc LIMIT 1";

            $res = $this->db->query($query);
            if ($res != false) {
                foreach ($res as $val) {
                    $price = $product["price"] - ($product["price"] * $val["reduction"]);
                    $productSpecialResource->setPrice($price);
                    $productSpecialResource->setDateFrom($val["from"]);
                    $productSpecialResource->setDateTo($val["to"]);

                    $productSpecialResource->setProduct(["id" => $productSRId]);
                    $array = $productSpecialResource->getAsArray();
                    $resultProductSpecialResult = querySRApi(API_ENDPOINT_PRODUCT_SPECIAL, $array, "POST", "responseBody", true);
                }
            }
        }
    }

    public function createProductClass() {
        $query = "SELECT id_product, count(*) AS c FROM ps_product_attribute GROUP BY id_product HAVING c >= 1";
        $result = $this->db->query($query);

        $productClassCSVArray = array();
        foreach ($result as $value) {
            $productClassCSV = array();
            $productResource = new \resources\Product();
            $productId = $value["id_product"];
            $productSRId = base64_encode("product-product_id=" . $productId);

            $productClass = array();

            $query = "SELECT name FROM ps_product_lang WHERE id_product = " . $productId . " AND id_lang = 6";
            $name = $this->db->findOneByQuery($query, "name");
            $productClass["name"] = $name . " - " . $value["id_product"];

            $query = "SELECT description FROM ps_product_lang WHERE id_product = " . $productId . " AND id_lang = 6";
            $description = $this->db->findOneByQuery($query, "description");
            $productClass["description"] = $description;


            $productClassResult = querySRApi("/productClasses", $productClass, 'POST', "responseBody", true);

            $productClassCSV["name"] = $productClass["name"];
            $productClassCSV["productClass_sr_id"] = $productClassResult["id"];

            $sku = $productResource->getProductSkuBySRId($productSRId);
            $productResource->setSku($sku);
            $productResource->setProductClass(array("id" => $productClassResult["id"]));
            querySRApi("/products/" . $productSRId, $productResource->getAsArray(), 'POST', "responseBody", true);

            $srProduct = array();
            $srProduct["productClass"] = array("id" => $productClassResult["id"]);
            $srProduct["parentProduct"] = array("id" => $productSRId);


            foreach ($this->getChildProducts() as $child) {
                $childrens = "";
                if ($child["parent_id"] == $productId) {
                    $srProduct["sku"] = $child["sku"];
                    $childrenSRId = $child["child_sr_id"];
                    $childrens .= $childrenSRId . "||";
                    $sr_product = querySRApi("/products/" . $childrenSRId, $srProduct, 'POST', "responseBody", true);
                }
                $childrens = substr($childrens, 0, -2);
            }
            $productClassCSV["childrens"] = $childrens;

            $productClassCSVArray[] = $productClassCSV;
        }

        $headers = ['name', 'productClass_sr_id', 'childrens'];
        $fp = fopen('data/productClass.csv', 'w');
        fputcsv($fp, $headers, ";");
        foreach ($productClassCSVArray as $fields) {
            fputcsv($fp, array_values($fields), ";");
        }
        fclose($fp);
    }

    public function insertProductClass() {

        $query = "SELECT parent_id, COUNT(*) c FROM catalog_product_relation GROUP BY parent_id HAVING c > 1";
        $result = $this->db->query($query);

        foreach ($result as $value) {
            $productClass = array();
            $srProduct = array();
            $productClass["id"] = $value["parent_id"];
            $productId = $value["parent_id"];
            $productSRId = base64_encode("product-product_id=" . $productId);

            $query = "SELECT value FROM catalog_product_entity_varchar WHERE entity_id = " . $productId . " AND attribute_id = 71";
            $name = $this->db->findOneByQuery($query, "value");
            if ($name != NULL) {
                $productClass["name"] = $name . " - " . $value["parent_id"];
            } else {
                $productClass["name"] = $value["parent_id"];
            }

//            $query = "SELECT value FROM catalog_product_entity_text WHERE entity_id = " . $productId . " AND attribute_id = 72";
//            $description = $this->db->findOneByQuery($query, "value");
//            if ($description != NULL) {
//                $description .= $this->getMoreInformation($productId);
//                $productClass["description"] = $description;
//            }

            $productClass["description"] = "";
            $productClassResult = querySRApi("/productClasses", $productClass, 'POST', "responseBody", false);

            $srProduct["productClass"] = array("id" => $productClassResult["id"]);
            $skuQuery = "SELECT sku FROM catalog_product_entity WHERE entity_id = " . $productId;
            $sku = $this->db->findOneByQuery($skuQuery, "sku");
            $srProduct["sku"] = $sku;
            $sr_product = querySRApi("/products/" . $productSRId, $srProduct, 'POST', "responseBody", true);

//            sout($productClassResult);
            $query = "SELECT * FROM catalog_product_relation WHERE parent_id = " . $value["parent_id"];
            $res = $this->db->query($query);
            if ($res != FALSE) {
                foreach ($res as $children) {
                    $productSRId = base64_encode("product-product_id=" . $children["child_id"]);

                    $srProduct = array();

                    $srProduct["parentProduct"] = array("id" => base64_encode("product-product_id=" . $children["parent_id"]));
                    $srProduct["productClass"] = array("id" => $productClassResult["id"]);

                    $skuQuery = "SELECT sku FROM catalog_product_entity WHERE entity_id = " . $children["child_id"];
                    $sku = $this->db->findOneByQuery($skuQuery, "sku");
                    $srProduct["sku"] = $sku;

                    $sr_product = querySRApi("/products/" . $productSRId, $srProduct, 'POST', "responseBody", TRUE);
                }
            }
        }
    }

    public function createProductAttribute() {
        $query = "SELECT * FROM ps_attribute_group_lang WHERE id_lang = 6";
        $valtozatok = array();
        $result = $this->db->query($query);
        foreach ($result as $value) {
            $egyValtozat = array();
            $egyValtozat["nev"] = $value["public_name"];
            $egyValtozat["cimke"] = str_replace("-", "_", slug($value["public_name"]));
            $egyValtozat["id_attribute_group"] = $value["id_attribute_group"];

            $query = "SELECT * FROM ps_attribute_lang AS pal WHERE id_lang = 6 AND id_attribute IN "
                    . "(SELECT id_attribute FROM ps_attribute WHERE id_attribute_group = " . $value["id_attribute_group"] . ")";
            $res = $this->db->query($query);
            if ($res != false) {
                $arr = array();
                foreach ($res as $val) {
                    $arr[] = array(
                        "name" => $val["name"],
                        "id_attribute" => $val["id_attribute"]
                    );
                }
                $egyValtozat["ertekek"] = $arr;
            }
            $valtozatok[] = $egyValtozat;
        }
//        sout($valtozatok);
//        die();
        for ($i = 0; $i < count($valtozatok); $i++) {
            $listAttribute = [];
            $listAttribute["type"] = "LIST";
            $listAttribute["name"] = $valtozatok[$i]["cimke"];
            $listAttribute["priority"] = "NORMAL";
            $listAttribute["sortOrder"] = "NORMAL";
            $listAttribute["required"] = 0;
            $listAttribute["presentation"] = "TEXT";

            $listAttributeResult = querySRApi("/listAttributes", $listAttribute, 'POST', "responseBody", false);

            $attributeDesc = Array
                (
                'name' => $valtozatok[$i]["nev"],
                'description' => "",
                'attribute' => array('id' => $listAttributeResult["id"]),
                'language' => array('id' => LANG_HU)
            );

            $attributeDescResult = querySRApi("/attributeDescriptions", $attributeDesc, 'POST', "responseBody", false);

            $query = "SELECT id_product, count(*) AS c FROM ps_product_attribute GROUP BY id_product HAVING c >= 1";
            $parents = $this->db->query($query);

            $productClassId = "";
            $productClassOriginalId = "";
            foreach ($parents as $parent) {

                $parentId = $parent["id_product"];
                $query = "SELECT id_attribute_group FROM ps_attribute WHERE id_attribute IN (
                            SELECT id_attribute FROM ps_product_attribute_combination WHERE id_product_attribute IN (
                                SELECT id_product_attribute FROM ps_product_attribute WHERE id_product = " . $parentId . "
                            )
                                ) LIMIT 1";

                $id_attribute_group = $this->db->findOneByQuery($query, "id_attribute_group");
                if ($id_attribute_group != "") {
                    foreach ($this->productClass as $productClass) {
                        if ($valtozatok[$i]["id_attribute_group"] == $id_attribute_group &&
                                $productClass["original_id"] == $parentId) {
                            $productClassAttributeRelations = Array
                                (
                                'productClass' => array('id' => $productClass["productClass_sr_id"]),
                                'attribute' => array('id' => $listAttributeResult["id"])
                            );
                            $productClassAttributeRelationsResult = querySRApi("/productClassAttributeRelations", $productClassAttributeRelations, 'POST', "responseBody", false);

                            $productClassResource = new resources\ProductClass();
                            $productClassResource->setId($productClass["productClass_sr_id"]);
                            $productClassResource->setFirstVariantSelectType("SELECT");
                            $productClassResource->setFirstVariantParameter(array("id" => $listAttributeResult["id"]));
                            $productClassResource->update(true);
                        }
                    }
                }
            }

            foreach ($valtozatok[$i]["ertekek"] as $value) {
                $name = $value["name"];
                $req = Array
                    (
                    'listAttribute' => array('id' => $listAttributeResult['id'])
                );
                $resultAV = querySRApi("/listAttributeValues", $req, 'POST', "responseBody", false);

                if (isset($resultAV["id"])) {
                    $req = Array
                        (
                        'listAttributeValue' => array('id' => $resultAV["id"]),
                        'language' => array('id' => LANG_HU),
                        'name' => $name
                    );
                    $resultAVD = querySRApi("/listAttributeValueDescriptions", $req, 'POST', "responseBody", false);



                    $query = "SELECT * FROM ps_product_attribute WHERE id_product_attribute IN "
                            . "(SELECT id_product_attribute FROM ps_product_attribute_combination "
                            . "WHERE id_attribute = " . $value["id_attribute"] . ")";

                    $resultQueryOption = $this->db->query($query);

                    if ($resultQueryOption != FALSE) {
                        foreach ($resultQueryOption as $option) {
                            foreach ($this->childProducts as $child) {
                                if ($option["id_product_attribute"] == $child["id_product_attribute"]) {
                                    $productSRIdFromCSV = $child["child_sr_id"];
                                    $productListAttributeValueRelations = [];
                                    $productListAttributeValueRelations["product"] = ["id" => $productSRIdFromCSV];
                                    $productListAttributeValueRelations["listAttributeValue"] = ["id" => $resultAV["id"]];
                                    $productClassAttributeRelationsResult = querySRApi("/productListAttributeValueRelations", $productListAttributeValueRelations, 'POST', "responseBody", false);
                                }
                            }
                        }
                    }
                } else {
                    echo 'attribute value error:';
                    print_r($resultAV);
                }
            }
        }
    }

    public function getProductClasses() {
//        $result = querySRApi("/productClasses?limit=200", [], 'GET', "responseBody", FALSE);
//        $array = [];
//        foreach ($result["items"] as $item) {
//            $id = getId($item["href"]);
//            $res = querySRApi("/productClasses/" . $id, [], 'GET', "responseBody", FALSE);
//            if (key_exists("name", $res)) {
//                $name = $res["name"];
//                $arr = explode("-", $name);
//                $original_id = $arr[count($arr) - 1];
//                $productClass = [];
//
//                $productClass["original_id"] = $original_id;
//                $productClass["id"] = $res["id"];
//                $productClass["name"] = $res["name"];
//                $array[] = $productClass;
//            }
//        }
//
//        return ($array);
    }

    public function insertRelatedProductRelations() {
        $query = "SELECT * FROM ps_accessory";
        $result = $this->db->query($query);
        foreach ($result as $value) {
            $productRelatedProductRelations = [];
            $productId = base64_encode("product-product_id=" . $value["id_product_1"]);
            $relatedProductId = base64_encode("product-product_id=" . $value["id_product_2"]);
            $productRelatedProductRelations["product"] = ["id" => $productId];
            $productRelatedProductRelations["relatedProduct"] = ["id" => $relatedProductId];
            $productRelatedProductRelationsResult = querySRApi("/productRelatedProductRelations", $productRelatedProductRelations, 'POST', "responseBody", true);
        }

        foreach ($this->childProducts as $child) {
            $query = "SELECT * FROM ps_accessory WHERE id_product_1 = " . $child["parent_id"];
            $result = $this->db->query($query);
            foreach ($result as $childValue) {
                $productRelatedProductRelations = [];
                $childProductId = $child["child_sr_id"];
                $relatedProductId = base64_encode("product-product_id=" . $childValue["id_product_2"]);
                $productRelatedProductRelations["product"] = ["id" => $childProductId];
                $productRelatedProductRelations["relatedProduct"] = ["id" => $relatedProductId];
                $productRelatedProductRelationsResult = querySRApi("/productRelatedProductRelations", $productRelatedProductRelations, 'POST', "responseBody", true);
            }
        }
    }

    public function insertProductTag() {
        foreach ($this->products as $product) {
            $productId = $product["id_product"];
            $productSRId = base64_encode("product-product_id=" . $productId);
            $productTag = new resources\ProductTag();
            $tags = "";
            $productTag->setProduct(array("id" => $productSRId));
            $productTag->setLanguage(array("id" => LANG_HU));

            $query = "SELECT * FROM ps_product_tag WHERE id_product = " . $productId;

            $result = $this->db->query($query);
            if ($result != false) {
                foreach ($result as $value) {
                    $queryTag = "SELECT name FROM ps_tag WHERE id_lang = 6 AND id_tag = " . $value["id_tag"];
                    $name = $this->db->findOneByQuery($queryTag, "name");
                    $tags .= $name . ",";
                }
            }
            if ($tags != "") {
                $tags = substr($tags, 0, -1);
                $productTag->setTags($tags);
                $productTag->insert(false);
            }
        }

        foreach ($this->getChildProducts() as $child) {
            $childProductSRId = $child["child_sr_id"];
            $productTag = new resources\ProductTag();
            $tags = "";
            $productTag->setProduct(array("id" => $childProductSRId));
            $productTag->setLanguage(array("id" => LANG_HU));

            $query = "SELECT * FROM ps_product_tag WHERE id_product = " . $child["parent_id"];
            $result = $this->db->query($query);
            if ($result != false) {
                foreach ($result as $value) {
                    $queryTag = "SELECT name FROM ps_tag WHERE id_lang = 6 AND id_tag = " . $value["id_tag"];
                    $name = $this->db->findOneByQuery($queryTag, "name");
                    $tags .= $name . ",";
                }
            }
            if ($tags != "") {
                $tags = substr($tags, 0, -1);
                $productTag->setTags($tags);
                $productTag->insert(false);
            }
        }
    }

    public function insertProductUrlAlias() {
        $urlAlias = new \resources\URLAlias();
        $urlAlias->deleteProductUrl();
        foreach ($this->products as $product) {
            $productId = $product["id_product"];
            $productSRId = base64_encode("product-product_id=" . $productId);

            $urlAlias->setType("PRODUCT");
            $urlAlias->setUrlAliasEntity(array("id" => $productSRId));
            $url = $productId . "-" . $product["link_rewrite"];
            $urlAlias->setUrlAlias($url);
            $urlAlias->insert(true, "POST");

            foreach ($this->childProducts as $child) {
                if ($child["parent_id"] == $productId) {
                    $parentId = $child["parent_id"];
                    $childProductSRId = $child["child_sr_id"];

                    $urlAlias->setType("PRODUCT");
                    $urlAlias->setUrlAliasEntity(array("id" => $childProductSRId));

                    $url = $parentId . "-" . $child["increment"] . "-" . $product["link_rewrite"];
                    $urlAlias->setUrlAlias($url);
                    $urlAlias->insert(true, "POST");
                }
            }
        }
    }

    public function insertCategoryUrlAlias() {
        $urlAliasResource = new \resources\URLAlias();
        $urlAliasResource->deleteCategoryUrl();

        $query = "SELECT * FROM ps_category";
        $categories = $this->db->query($query);
        foreach ($categories as $category) {

            $categoryId = $category["id_category"];
            $categorySRId = base64_encode("category-category_id=" . $categoryId);

            $query = "SELECT link_rewrite FROM ps_category_lang WHERE id_lang = 6 AND id_category = " . $categoryId;
            $link_rewrite = $this->db->findOneByQuery($query, "link_rewrite");
            $urlAlias = $categoryId . "-" . $link_rewrite;

            $urlAliasResource->setType("CATEGORY");
            $urlAliasResource->setUrlAliasEntity(["id" => $categorySRId]);
            $urlAliasResource->setUrlAlias($urlAlias);
            $urlAliasResource->insert(true, "POST");
        }
    }

    public function create301Product() {
        $csvArray = array();
        foreach ($this->products as $product) {
            $productId = $product["id_product"];
            $url = $productId . "-" . $product["link_rewrite"];
            $csv["old_url"] = $url . ".html";
            $csv["new_url"] = $url;
            $csv["order"] = 0;
            $csvArray[] = $csv;
//            foreach ($this->childProducts as $child) {
//                if ($child["parent_id"] == $productId) {
//                    $parentId = $child["parent_id"];
//                    $urlChild = $parentId . "-" . $child["increment"] . "-" . $product["link_rewrite"];
//                    $csvArray["old_url"] = $urlChild . ".html";
//                    $csvArray["new_url"] = $urlChild;
//                }
//            }
        }
        $headers = ['old_url', 'new_url', 'order'];
        $fp = fopen('data/sutnifozniProductURL.csv', 'w');
        fputcsv($fp, $headers, ";");
        foreach ($csvArray as $fields) {
            fputcsv($fp, array_values($fields), ";");
        }
        fclose($fp);
    }

    private function createImageUrlFromImageId($imgId) {
        $array = str_split($imgId);
        $dir = "p/";
        foreach ($array as $value) {
            $dir .= $value . "/";
        }

        $url = $dir . $imgId . "-watermark.jpg";

        return $url;
    }

    private function removoIfMatch($string, $character) {
        if (substr($string, -1) === $character) {
            return substr($string, 0, -1);
        } else {
            return $string;
        }
    }

}
