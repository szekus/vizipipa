<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace db;

/**
 * Description of StatusToSubmitted
 *
 * @author tamas
 */
class StatusToSubmitted extends Database {

    public static function instance() {
        static $inst = null;
        if ($inst === null) {
            $inst = new StatusToSubmitted();
        }
        return $inst;
    }

    public function __construct() {
        parent::__construct();
        $this->data = array("id", "status_id");
        $this->tableName = "status_to_submitted";
    }

    public function getSelectedStatusId() {
        $query = "SELECT status_id FROM $this->tableName LIMIT 1";
        return $this->findOneByQuery($query, "status_id");
    }

}
